import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)

@app.route('/books', methods=['GET'])
def get_users():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='onlinebook_db')
        
    cur = conn.cursor()
    cur.execute("select * from books LIMIT 20")
    output = cur.fetchall()

    print(type(output)); #this will print tuple	

    for rec in output:
        print(rec);
        
    # To close the connection
    conn.close()

    return jsonify(output);

@app.route('/books/id', methods=['GET'])
def read():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='onlinebook_db')
    cur = conn.cursor()
    id = int(request.args.get('id'))
    cur.execute(f"select * from books WHERE id = {id}");

    output = cur.fetchall()

    print(type(output)); #this will print tuple

    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);


@app.route('/books', methods=['DELETE'])
def deleteRecord():
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='onlinebook_db')
    cur = conn.cursor()
    id = int(request.args.get('id'));

    query = f"delete from books where id = {id}";
    #print(query)
    res = cur.execute(query);
    conn.commit();
    print(cur.rowcount, "record(s) deleted")

    return "Record deleted sussesfully"

@app.route('/books', methods=['POST'])	
def insertRecord(): 
    conn= pymysql.connect(host='localhost',user='phpuser',password= "%TGBbgt5",db='onlinebook_db')

    #get raw json values
    raw_json = request.get_json();
    title = raw_json['title'];
    author = raw_json['author'];
    price = raw_json['price'];
    publisher = raw_json['publisher'];


    sql=f"INSERT INTO books (id, title, author, price, publisher, publish_date) VALUES (NULL,'{title}','{author}','{price}','{publisher}',NOW())";
    cur= conn.cursor()

    cur.execute(sql);
    conn.commit()
    return "Record inserted Succesfully"

@app.route('/books', methods=['PUT'])
def updateRecord():
    conn= pymysql.connect(host='localhost',user='phpuser',password= "%TGBbgt5",db='onlinebook_db')
    
    raw_json = request.get_json();
    
    #print(type(raw_json));
    
    raw_json = request.get_json();
    id = raw_json['id'];
    title = raw_json['title'];
    author = raw_json['author'];
    price = raw_json['price'];
    publisher = raw_json['publisher'];
    

    sql_update_quary=(f"UPDATE onlinebook_db.books SET title = '{title}', author = '{author}',price = '{price}', publisher = '{publisher}' WHERE id = '{id}'");
    cur= conn.cursor()
    cur.execute(sql_update_quary);
    conn.commit()
    return "Record Updated Sussecfully"; 
	
	
if __name__ == "__main__":
    app.run(debug=True);
